function tratarString(string)
  stringTratada = string
  stringTratada = replace(stringTratada, r"á|à|â|ã"i => "a")
  stringTratada = replace(stringTratada, r"é|è|ê"i => "e")
  stringTratada = replace(stringTratada, r"í|ì|î"i => "i")
  stringTratada = replace(stringTratada, r"ó|ò|ô|õ"i => "o")
  stringTratada = replace(stringTratada, r"ú|ù|û"i => "u")
  stringTratada = replace(stringTratada, r"ç"i => "c")
  stringTratada = replace(stringTratada, r"[A-Z]" => lowercase)
  stringTratada = replace(stringTratada, r"[^a-z0-9]" => "")
  return stringTratada
end

function palindromo(string)
  stringTratada = tratarString(string)
  indexEspelho = lastindex(stringTratada)
  index = firstindex(stringTratada)
  for i in 1:length(stringTratada)÷2
    (stringTratada[index] != stringTratada[indexEspelho]) && return false
    index = nextind(stringTratada, index)
    indexEspelho = prevind(stringTratada, indexEspelho)
  end
  return true
end

    
    
